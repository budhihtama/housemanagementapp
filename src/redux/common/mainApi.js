import axios from 'axios';

const baseUrl = 'http://10.0.2.2:8000';

export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: baseUrl + '/auth/login',
    data: dataLogin,
  });
}

export function apiProfile(headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/profile',
    headers,
  });
}
export function apiEditProfile(payload, headers) {
  return axios({
    method: 'PATCH',
    url: baseUrl + '/profile/1',
    data: payload,
    headers,
  });
}
export function apiGetHouse(headers) {
  return axios({
    method: 'GET',
    url: baseUrl + '/house',
    headers,
  });
}
export function apiAddHouse(payload, headers) {
  return axios({
    method: 'POST',
    url: baseUrl + '/house',
    data: payload,
    headers,
  });
}
