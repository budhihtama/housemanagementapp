import {takeLatest, put} from 'redux-saga/effects';
import {getHeaders, removeToken} from '../common/auth';
import {apiGetHouse, apiAddHouse} from '../common/mainApi';

function* getHouse(action) {
  try {
    const headers = yield getHeaders();
    const resGetHouse = yield apiGetHouse(headers);
    yield put({type: 'GET_HOUSE_SUCCESS', payload: resGetHouse.data});
    console.log('oo', resGetHouse.data);
  } catch (e) {
    console.error('e', e);
    yield put({type: 'GET_HOUSE_FAILED'});
  }
}
function* addHouse(action) {
  try {
    const headers = yield getHeaders();
    const resAddHouse = yield apiAddHouse(action.payload, headers);
    yield put({type: 'ADD_HOUSE_SUCCESS', payload: resAddHouse.data});
    yield put({type: 'GET_HOUSE'});
    console.log('oo', resAddHouse.data);
  } catch (e) {
    console.error('e', e);
    yield put({type: 'ADD_HOUSE_FAILED'});
  }
}

function* house() {
  yield takeLatest('GET_HOUSE', getHouse);
  yield takeLatest('ADD_HOUSE', addHouse);
}

export default house;
