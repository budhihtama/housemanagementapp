import {all} from 'redux-saga/effects';
import auth from './auth';
import profile from './profile';
import house from './house';

export default function* rootSaga() {
  yield all([auth(), profile(), house()]);
}
