import {takeLatest, put} from 'redux-saga/effects';
import {saveAccountId, saveToken} from '../common/auth';
import {apiLogin} from '../common/mainApi';

function* login(action) {
  try {
    const resLogin = yield apiLogin(action.payload);

    if (resLogin && resLogin.data) {
      yield saveToken(resLogin.data.access_token);
      // yield saveAccountId(resLogin.data.id);

      yield put({type: 'LOGIN_SUCCESS'});
      console.log(resLogin.data);
      yield put({type: 'GET_PROFILE'});
      yield put({type: 'GET_HOUSE'});
    } else {
      yield put({type: 'LOGIN_FAILED'});
    }
  } catch (e) {
    console.error('e', e);
    yield put({type: 'LOGIN_FAILED'});
    alert('login gagal');
  }
}

function* auth() {
  yield takeLatest('LOGIN', login);
}

export default auth;
