import {takeLatest, put} from 'redux-saga/effects';
import {getHeaders, removeToken} from '../common/auth';
import {apiProfile, apiEditProfile} from '../common/mainApi';

function* getProfile(action) {
  try {
    const headers = yield getHeaders();
    const resGetProfile = yield apiProfile(headers);
    yield put({type: 'GET_PROFILE_SUCCESS', payload: resGetProfile.data});
    console.log('oo', resGetProfile.data);
  } catch (e) {
    console.error('e', e);
    yield put({type: 'GET_PROFILE_FAILED'});
  }
}
function* editProfile(action) {
  try {
    const headers = yield getHeaders();
    const resEditProfile = yield apiEditProfile(action.payload, headers);
    yield put({type: 'EDIT_PROFILE_SUCCESS', payload: resEditProfile.data});
    yield put({type: 'GET_PROFILE'});
    console.log('oo', resEditProfile.data);
  } catch (e) {
    console.error('e', e);
    yield put({type: 'EDIT_PROFILE_FAILED'});
  }
}
function* logout() {
  try {
    yield removeToken();
  } catch (e) {
    console.error('e', e);
  }
}

function* profile() {
  yield takeLatest('GET_PROFILE', getProfile);
  yield takeLatest('EDIT_PROFILE', editProfile);
  yield takeLatest('LOGOUT', logout);
}

export default profile;
