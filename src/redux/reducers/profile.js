const initialState = {
  data: [],
  isLoading: false,
};

const profile = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PROFILE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'GET_PROFILE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'GET_PROFILE_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'EDIT_PROFILE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'EDIT_PROFILE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'EDIT_PROFILE_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
};

export default profile;
