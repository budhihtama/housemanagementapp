import {combineReducers} from 'redux';
import auth from '../reducers/auth';
import datahouse from '../reducers/datahouse';
import profile from '../reducers/profile';
import house from '../reducers/house';
import addhouse from '../reducers/addhouse';

export default combineReducers({
  auth,
  datahouse,
  profile,
  house,
  addhouse,
});
