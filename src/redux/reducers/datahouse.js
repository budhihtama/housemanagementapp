const initialState = {
  homeList: [
    {
      id: 0,
      title: 'Jakarta',
      address: 'Jalan Menteng Jaya No. 11 Menteng Jakarta Pusat',
      total: 1900000,
      dataBills: [
        {
          title: 'Electricity',
          tagihan: 1000000,
          duedate: '02/02/2021',
        },
        {
          title: 'Utilities',
          tagihan: 600000,
          duedate: '02/02/2021',
        },
        {
          title: 'Internet & TV',
          tagihan: 300000,
          duedate: '02/02/2021',
        },
      ],
      image: [
        {
          url: require('../../assets/image/jakarta0.jpg'),
        },
        {
          url: require('../../assets/image/jakarta1.jpg'),
        },
        {
          url: require('../../assets/image/jakarta2.jpg'),
        },
      ],
    },
    {
      id: 1,
      title: 'Bogor',
      address: 'Jalan Raya Bogor',
      total: 9200000,
      dataBills: [
        {
          title: 'Electricity',
          tagihan: 600000,
          duedate: '02/02/2021',
        },
        {
          title: 'Utilities',
          tagihan: 220000,
          duedate: '02/02/2021',
        },
        {
          title: 'Internet & TV',
          tagihan: 100000,
          duedate: '02/02/2021',
        },
      ],
      image: [
        {
          url: require('../../assets/image/bogor0.jpg'),
        },
        {
          url: require('../../assets/image/bogor1.jpg'),
        },
        {
          url: require('../../assets/image/bogor2.jpg'),
        },
      ],
    },
    {
      id: 2,
      title: 'Tangerang',
      address: 'Jalan Raya BSD',
      total: 2100000,
      dataBills: [
        {
          title: 'Electricity',
          tagihan: 900000,
          duedate: '02/02/2021',
        },
        {
          title: 'Utilities',
          tagihan: 700000,
          duedate: '02/02/2021',
        },
        {
          title: 'Internet & TV',
          tagihan: 500000,
          duedate: '02/02/2021',
        },
      ],
      image: [
        {
          url: require('../../assets/image/tangerang0.jpg'),
        },
        {
          url: require('../../assets/image/tangerang1.jpg'),
        },
        {
          url: require('../../assets/image/tangerang2.jpg'),
        },
      ],
    },
  ],
};

const datahouse = (state = initialState, action) => {
  switch (action.type) {
    case 'GET': {
      return {
        ...state,
        isLoggedIn: true,
      };
    }
    case 'GET_SUCCESS': {
      return {
        ...state,
        isLoggedIn: true,
      };
    }

    default:
      return state;
  }
};

export default datahouse;
