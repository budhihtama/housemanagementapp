const initialState = {
  data: [],
  isLoading: false,
};

const house = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_HOUSE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'GET_HOUSE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'GET_HOUSE_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'EDIT_HOUSE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'EDIT_HOUSE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'EDIT_HOUSE_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
};

export default house;
