const initialState = {
  data: [],
  isLoading: false,
};

const addHouse = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_HOUSE': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'ADD_HOUSE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'ADD_HOUSE_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }

    default:
      return state;
  }
};

export default addHouse;
