import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigator from './MainNavigator';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import LoginScreen from '../screens/LoginScreen';
import HomeListScreen from '../screens/HomeListScreen';
import HomeDetailScreen from '../screens/HomeDetailScreen';
import ComplaintScreen from '../screens/ComplaintScreen';

const Stack = createStackNavigator();

function AppStack(props) {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {isLoggedIn ? (
          <>
            <Stack.Screen
              options={{
                headerShown: false,
              }}
              name="Main"
              component={MainNavigator}
            />
            <Stack.Screen
              options={{
                headerShown: false,
              }}
              name="MyHome"
              component={HomeListScreen}
            />
            <Stack.Screen
              options={{
                headerShown: false,
              }}
              name="HomeDetail"
              component={HomeDetailScreen}
            />
            <Stack.Screen
              options={{
                headerShown: false,
              }}
              name="Complain"
              component={ComplaintScreen}
            />
          </>
        ) : (
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name="Login"
            component={LoginScreen}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default AppStack;
