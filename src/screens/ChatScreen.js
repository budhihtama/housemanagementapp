import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Linking,
  TouchableOpacity,
  Image,
} from 'react-native';

export default function ChatScreen() {
  const listContact = [
    {
      id: 0,
      title: 'Builder',
      number: '+6287782203934',
      image: 'https://placeimg.com/640/480/tech/nature',
    },
    {
      id: 1,
      title: 'Bartender',
      number: '+6287782203934',
      image: 'https://placeimg.com/640/480/tech/grayscale',
    },
    {
      id: 2,
      title: 'Carpenter',
      number: '+6287782203934',
      image: 'https://placeimg.com/640/480/tech/sepia',
    },
    {
      id: 3,
      title: 'Craftsman',
      number: '+6287782203934',
      image: 'https://placeimg.com/640/480/tech/tech',
    },
  ];
  return (
    <View style={{alignItems: 'center', marginVertical: '5%'}}>
      {listContact.map((item) => (
        <TouchableOpacity
          style={styles.btn}
          onPress={() => Linking.openURL(`sms:${item.number}?text=mas tolong`)}
          key={item.id}>
          <Image style={styles.img} source={{uri: item.image}} />
          <Text style={styles.txt}>{item.title}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}
const styles = StyleSheet.create({
  btn: {
    width: '90%',
    padding: '5%',
    borderRadius: 20,
    marginVertical: '1%',
    flexDirection: 'row',
  },
  txt: {
    fontWeight: 'bold',
    alignSelf: 'center',
    marginHorizontal: '5%',
  },
  img: {
    width: 50,
    height: 50,
    borderRadius: 50,
  },
});
