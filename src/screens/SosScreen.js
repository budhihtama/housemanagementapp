import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Linking,
} from 'react-native';

export default function SosScreen() {
  const listData = [
    {
      id: 0,
      title: 'Call Police',
      number: 911,
    },
    {
      id: 1,
      title: 'Call Ambulance',
      number: 112,
    },
    {
      id: 2,
      title: 'Call Fire Figther',
      number: 111,
    },
    {
      id: 3,
      title: 'Call Centre',
      number: 123,
    },
  ];

  return (
    <View style={{alignItems: 'center', marginVertical: '5%'}}>
      {listData.map((item) => (
        <TouchableOpacity
          onPress={() => Linking.openURL(`tel:${item.number}`)}
          style={styles.btn}
          key={item.id}>
          <Text style={styles.txt}>{item.title}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}
const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#b3c2b0',
    width: '90%',
    padding: '5%',
    borderRadius: 20,
    marginVertical: '1%',
  },
  txt: {
    fontWeight: 'bold',
  },
});
