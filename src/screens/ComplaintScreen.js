import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

export default function ComplaintScreen() {
  return (
    <View>
      <View style={styles.header}></View>
      <View
        style={{
          width: '80%',
          height: '25%',
          borderRadius: 10,
          backgroundColor: 'white',
          justifyContent: 'center',
          alignSelf: 'center',
          top: -20,
          elevation: 20,
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 30, textAlign: 'center'}}>
          Your Complaints
        </Text>
      </View>
      <View>
        <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 15}}>
          Put Your Complain Here!
        </Text>
        <TextInput style={styles.txtInput} placeholder="write Here" />
      </View>
      <TouchableOpacity style={styles.btnSubmit}>
        <Text style={styles.txtSubmit}>SUBMIT</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#e7e7de',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSubmit: {
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    width: '80%',
    alignSelf: 'center',
    marginVertical: '5%',
  },
  txtSubmit: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  txtInput: {
    borderWidth: 1,
    width: '80%',
    alignSelf: 'center',
    padding: 10,
    borderRadius: 10,
  },
});
