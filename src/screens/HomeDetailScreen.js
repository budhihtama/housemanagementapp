import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  Dimensions,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 0.4);

export default function HomeDetailScreen({route}) {
  const dataHome = route.params.item;
  const photo = route.params.item.photo;
  const keyExtractor = (item) => item.id;
  const renderImage = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <Image style={styles.Img} source={{uri: item.url}} />
      </View>
    );
  };
  return (
    <View>
      <FlatList
        pagingEnabled={true}
        horizontal={true}
        data={photo}
        renderItem={renderImage}
        keyExtractor={keyExtractor}
        style={styles.CONTAINER}
        style={styles.FLATLIST}
      />
      <View style={styles.bill}>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{dataHome.name}</Text>
        <Text>{dataHome.address}</Text>
        <Text style={{fontSize: 30, marginVertical: '3%'}}>Bill Details</Text>
      </View>
      {dataHome.billing.map((item) => (
        <View style={styles.downContainer}>
          <View style={styles.cardBill}>
            <Text style={styles.titleTxt}>{item.type}</Text>
            <Text style={styles.txtBills}>Due Date: {item.duedate}</Text>
          </View>
          <Text style={styles.txtNumber}>Bill: Rp.{item.bill}</Text>
          <TouchableOpacity
            style={{
              paddingVertical: '1%',
              backgroundColor: 'green',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
            }}>
            <Text
              style={{textAlign: 'center', fontSize: 20, fontWeight: 'bold'}}>
              Pay
            </Text>
          </TouchableOpacity>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  home: {
    paddingHorizontal: '5%',
  },
  CONTAINER: {
    flexDirection: 'row',
    width: SLIDER_WIDTH,
  },
  FLATLIST: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  Img: {
    width: SLIDER_WIDTH,
    height: ITEM_HEIGHT,
  },
  bill: {
    paddingHorizontal: '5%',
  },
  downContainer: {
    backgroundColor: '#e7e7de',
    marginHorizontal: '5%',
    borderRadius: 10,
    marginVertical: '2%',
  },
  txtBills: {
    fontSize: 15,
    paddingRight: 10,
  },
  titleTxt: {
    fontSize: 27,
    paddingLeft: 10,
  },
  cardBill: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtNumber: {
    fontSize: 20,
    paddingLeft: 10,
    paddingVertical: '1%',
  },
});
