import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

function LoginScreen(props) {
  const [name, setName] = useState('budi@gmail.com');
  const [password, setPassword] = useState('budi');
  const dispatch = useDispatch();
  const Login = () => {
    const data = {
      email: name,
      password,
    };
    dispatch({type: 'LOGIN', payload: data});
  };

  return (
    <View>
      <Image
        style={{
          width: 100,
          height: 100,
          alignSelf: 'center',
          marginTop: 30,
          marginBottom: 50,
        }}
        source={require('../assets/image/home.png')}
      />
      <Text style={{marginLeft: '10%', fontSize: 28, fontWeight: 'bold'}}>
        WELCOME
      </Text>
      <Text style={{marginLeft: '10%', fontSize: 18}}>
        Home Management System
      </Text>
      <View
        style={{
          flexDirection: 'column',
          justifyContent: 'center',
          height: '50%',
        }}>
        <TextInput
          style={styles.txtInput}
          placeholder="Email or Username"
          value={name}
          onChangeText={(text) => setName(text)}
        />
        <TextInput
          style={styles.txtInput}
          placeholder="Password"
          value={password}
          secureTextEntry={true}
          onChangeText={(text) => setPassword(text)}
        />
        <View style={styles.button}>
          <TouchableOpacity onPress={() => Login()}>
            <Text style={styles.txtSubmit}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

export default LoginScreen;

const styles = StyleSheet.create({
  txtInput: {
    backgroundColor: '#e7e7de',
    borderRadius: 10,
    borderWidth: 4,
    borderColor: 'gray',
    width: '80%',
    height: '18%',
    alignSelf: 'center',
    marginVertical: 10,
    paddingLeft: 10,
  },
  button: {
    width: '80%',
    borderRadius: 10,
    borderWidth: 4,
    alignSelf: 'center',
    borderColor: 'gray',
    marginVertical: 10,
  },
  txtSubmit: {
    textAlign: 'center',
  },
});
