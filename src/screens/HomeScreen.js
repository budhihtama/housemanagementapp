import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Card from '../components/Card';
import Header from '../components/Header';
import Slide from '../components/Slide';
import ButtonNeeds from '../components/ButtonNeeds';

export default function HomeScreen() {
  return (
    <View style={styles.container}>
      <Header />
      <Card />
      <Slide />
      <Text style={styles.txt}>Your Needs</Text>
      <ButtonNeeds />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e7e7de',
    height: '100%',
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: '10%',
    marginVertical: 10,
  },
});
