import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {sub} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSelector, useDispatch} from 'react-redux';

function ProfileScreen() {
  const [name, setName] = useState();
  const [address, setAddress] = useState();
  const [email, setEmail] = useState();
  const [phone, setPhone] = useState();
  const [edit, setEdit] = useState(false);
  const profile = useSelector((state) => state.profile.data[0]);
  const dispatch = useDispatch();
  useEffect(() => {
    if (profile) {
      setName(profile.name);
      setAddress(profile.address);
      setEmail(profile.email);
      setPhone(profile.phone);
    }
  }, [profile]);
  const listProfile = [
    {
      id: 0,
      title: 'Name',
      data: name,
      onChangeText: (text) => setName(text),
    },
    {
      id: 1,
      title: 'Address',
      data: address,
      onChangeText: (text) => setAddress(text),
    },
    {
      id: 2,
      title: 'Email',
      data: email,
      onChangeText: (text) => setEmail(text),
    },
    {
      id: 3,
      title: 'Phone Number',
      data: phone,
      onChangeText: (text) => setPhone(text),
    },
  ];
  const submit = () => {
    const data = {
      id: profile.id,
      name,
      address,
      email,
      phone,
    };
    dispatch({type: 'EDIT_PROFILE', payload: data});
  };
  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.txt}>My Profile</Text>
        {edit == false ? (
          <TouchableOpacity onPress={() => setEdit(!edit)}>
            <Icon style={{}} name="pencil" color={'black'} size={20} />
          </TouchableOpacity>
        ) : null}
      </View>
      {listProfile.map((item) => (
        <View key={item.id} style={{marginVertical: 10, width: '90%'}}>
          <Text>{item.title}</Text>
          <TextInput
            style={{color: 'black'}}
            key={item.id}
            value={item.data}
            onChangeText={item.onChangeText}
            placeholder={item.data}
            editable={edit}
          />
        </View>
      ))}
      {edit == true ? (
        <TouchableOpacity
          onPress={() => {
            submit();
            setEdit(!edit);
          }}
          style={styles.btn}>
          <Text style={{color: 'black', textAlign: 'center'}}>SAVE</Text>
        </TouchableOpacity>
      ) : null}
      <TouchableOpacity
        style={styles.btnLogout}
        onPress={() => dispatch({type: 'LOGOUT'})}>
        <Text style={{textAlign: 'center'}}>LOGOUT</Text>
      </TouchableOpacity>
    </View>
  );
}
export default ProfileScreen;
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginVertical: 20,
  },
  txt: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: 20,
    marginHorizontal: '2%',
  },
  btn: {
    width: '80%',
    borderRadius: 10,
    borderWidth: 4,
    alignSelf: 'center',
    borderColor: 'gray',
    marginVertical: 10,
  },
  btnLogout: {
    width: '80%',
    borderRadius: 10,
    borderWidth: 4,
    alignSelf: 'center',
    borderColor: 'gray',
    marginVertical: 10,
  },
});
