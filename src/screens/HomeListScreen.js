import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import Headers from '../components/Header';

export default function HomeList({navigation}) {
  const home = useSelector((state) => state.house.data);
  const submit = () => {};

  return (
    <>
      <Headers style={styles.header} />
      <View style={styles.container}>
        {home.map((item) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('HomeDetail', {item})}
            style={styles.btn}
            key={item.id}>
            <Text style={styles.txt}>{item.name}</Text>
          </TouchableOpacity>
        ))}
        <View style={styles.btnAdd}>
          <TouchableOpacity>
            <Text
              style={{fontSize: 20, alignSelf: 'center', fontWeight: 'bold'}}>
              Add Home
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  btn: {
    backgroundColor: '#b3c2b0',
    width: '90%',
    padding: '3%',
    borderRadius: 20,
    marginVertical: '2%',
    marginTop: 30,
    fontWeight: 'bold',
  },
  btnAdd: {
    backgroundColor: '#b3c2b0',
    width: '90%',
    padding: '3%',
    borderRadius: 20,
    marginVertical: '50%',
    fontWeight: 'bold',
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center',
  },
});
