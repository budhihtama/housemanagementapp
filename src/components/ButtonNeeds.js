import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

export default function ButtonNeeds() {
  const navigation = useNavigation();
  const listButton = [
    {
      id: 0,
      title: 'Complaints',
      icon: 'warning',
      onPress: 'Complain',
    },
    {
      id: 1,
      title: 'My Home',
      icon: 'home',
      onPress: 'MyHome',
    },
  ];
  return (
    <View style={styles.container}>
      {listButton.map((item) => (
        <View key={item.id} style={{alignItems: 'center'}}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => navigation.navigate(item.onPress)}>
            <Ionicon name={item.icon} size={40} color="black" />
          </TouchableOpacity>
          <Text>{item.title}</Text>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginHorizontal: '5%',
    justifyContent: 'space-evenly',
  },
  btn: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 5,
    width: 60,
    alignItems: 'center',
    elevation: 4,
  },
});
