import React, {useState} from 'react';
import {
  Text,
  View,
  Dimensions,
  StyleSheet,
  Image,
  FlatList,
  ScrollView,
} from 'react-native';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(ITEM_WIDTH * 0.4);

const Slide = () => {
  const data = [
    {
      id: '0',
      image: 'https://placeimg.com/640/480/tech',
    },
    {
      id: '1',
      image: 'https://placeimg.com/640/480/tech/grayscale',
    },
    {
      id: '2',
      image: 'https://placeimg.com/640/480/arch/sepia',
    },
  ];

  const [index, setIndex] = useState();
  const keyExtractor = (item) => item.id;

  const renderImage = ({item}) => {
    return (
      <View style={styles.itemContainer}>
        <Image style={styles.Img} source={{uri: item.image}} />
      </View>
    );
  };

  return (
    <View style={{alignItems: 'center', marginVertical: 20}}>
      <ScrollView style={{width: SLIDER_WIDTH * 0.9}}>
        <FlatList
          pagingEnabled={true}
          horizontal={true}
          data={data}
          renderItem={renderImage}
          keyExtractor={keyExtractor}
          style={styles.CONTAINER}
          style={styles.FLATLIST}
        />
      </ScrollView>
    </View>
  );
};
export default Slide;

const styles = StyleSheet.create({
  itemContainer: {},
  CONTAINER: {
    flexDirection: 'row',
    width: SLIDER_WIDTH * 0.9,
  },
  FLATLIST: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  Img: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    borderRadius: 10,
  },
});
