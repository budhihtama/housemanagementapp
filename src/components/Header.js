import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Modal} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useSelector} from 'react-redux';

export default function Header() {
  const data = useSelector((state) => state.profile.data[0]);
  const [profile, setProfile] = useState('anonymous')
  const [modalvisible, setModalvisible] = useState(false);

  useEffect(()=> {
    if(data != null){
      setProfile(data.name)
    }
  },[data])

  const getCurrentDate = () => {
    let monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    var date = new Date().getDate();
    var month = monthNames[new Date().getMonth()];
    var year = new Date().getFullYear();
    //Alert.alert(date + '-' + month + '-' + year);
    // You can turn it in to your desired format
    return `${month} ${date}, ${year}`; //format: month dd, yyyy;
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <View>
          <Text>{getCurrentDate()}</Text>
          <Text style={{fontSize: 20}}>Hello {profile}</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            setModalvisible(true);
          }}>
          <Icon style={{}} name="bell" size={20} color="black" />
        </TouchableOpacity>
        <Modal animationType="fade" transparent={true} visible={modalvisible}>
          <View style={styles.modalcontent}>
            <Text
              style={{alignSelf: 'center', fontSize: 20, marginTop: 5}}
              onPress={() => setModalvisible(false)}>
              Notification
            </Text>
          </View>
        </Modal>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: '15%',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    elevation: 40,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center',
  },
  modalcontent: {
    backgroundColor: 'white',
    height: '100%',
  },
});
