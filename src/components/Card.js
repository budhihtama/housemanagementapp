import React,{useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import {Rupiah} from '../function'

export default function Card(props) {
  const data = useSelector((state) => state.house.data)
  const [bill, setBill] = useState(0)
 
  useEffect(() =>{
    const temp = []
    const temp2 = []
    if(data != null){
     for(const item of data){
       temp.push(item.billing)
       if(temp.length > 0){
         for(const item of temp[0]){
            temp2.push(item.bill)
            let sum = temp2.reduce((a,b) => {
              return a + b;
            },0)
            setBill(sum)
         }
       }
     }
    }
  },[data])
  console.log(bill)

  

  return (
    <>
      <View style={styles.container}>
        <View style={styles.leftSide}>
          <Text style={{fontSize: 25, color: 'white'}}>Total Invoice</Text>
          <Text style={{color: 'white'}}>Status</Text>
        </View>
        <View style={styles.rightSide}>
          <Text style={{fontSize: 25, fontWeight: 'bold', color: 'white'}}>
            Rp.{' '}
            {Rupiah(bill)}
          </Text>
          <Text style={styles.paid}>paid</Text>
        </View>
      </View>
      <View style={styles.containerDown}>
        <Text style={{fontSize: 15}}>Due Date</Text>
        <Text style={{fontSize: 15}}>01/01/2021</Text>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#b3c2b0',
    width: '90%',
    height: '15%',
    padding: 10,
    marginHorizontal: 10,
    marginTop: 20,
    alignSelf: 'center',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  leftSide: {
    padding: 5,
    justifyContent: 'space-between',
  },
  rightSide: {
    padding: 5,
    justifyContent: 'space-between',
  },
  containerDown: {
    backgroundColor: 'white',
    color: '#b3c2b0',
    width: '90%',
    height: '5%',
    paddingHorizontal: '5%',
    paddingVertical: 5,
    alignSelf: 'center',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  paid: {
    alignSelf: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 15,
    borderColor: 'white',
    paddingHorizontal: 20,
    fontWeight: 'bold',
    color: '#b3c2b0',
  },
});
