import React from 'react';
import 'react-native-gesture-handler';
import {Provider} from 'react-redux'
import store from './src/redux/store'
import AppStack from './src/navigator/AppStack'

const App = () => {
  return (
    <Provider store={store}>
      <AppStack/>
    </Provider>
  );
};

export default App;
